package org.icepdf.ri.common.utility.util.print;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.*;

import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;

/**
 * Created by Ihor on 6/2/2017.
 */
public class PrintUtil {

    static private final int MAX_SIZE = 70;

    static public File createTempFile(String text, String splitter) throws IOException {
        String[] lines = text.split(splitter);


        File temp = File.createTempFile("tempfile", ".tmp");
        //write it
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(temp), "UTF-8"));
        for(String line : lines) {
            int start = 0;
            if(line.length() > MAX_SIZE) {
                do {
                    bw.write(line.substring(start, MAX_SIZE));
                    bw.newLine();
                    start +=MAX_SIZE;
                } while (line.length() < MAX_SIZE);
            }
            bw.write(line.substring(start));
            bw.newLine();
        }
        bw.close();
        return temp;
    }

    static public void print(PrintService service, String text, String splitter) throws IOException, PrintException {
        print(service, text, splitter, MediaSizeName.ISO_A4);
    }

    public static void print(PrintService service, String text, String splitter, MediaSizeName mediaSizeName) throws IOException, PrintException {
        File temp = createTempFile(text, splitter);

        FileInputStream in = new FileInputStream(temp);

        PrintRequestAttributeSet attr_set = new HashPrintRequestAttributeSet();
        attr_set.add(mediaSizeName);
        attr_set.add(new Copies(1));

        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc doc = new SimpleDoc(in, flavor, null);

        DocPrintJob job = service.createPrintJob();
        PrintJobWatcher pjw = new PrintJobWatcher(job);
        job.print(doc, attr_set);
        pjw.waitForDone();
        in.close();
    }

    public static void print(BufferedImage image) {
        PrinterJob printJob = PrinterJob.getPrinterJob();
        printJob.setPrintable(new ImagePrintable(printJob, image));

        if (printJob.printDialog()) {
            try {
                printJob.print();
            } catch (PrinterException prt) {
                prt.printStackTrace();
            }
        }
    }
}
