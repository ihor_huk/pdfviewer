package org.icepdf.ri.common.utility.util;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by Ihor on 5/31/2017.
 */
public class CustomMouseListener implements MouseListener {
    public void mouseClicked(MouseEvent evt) {
        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            evt.consume();
            TouchKeyboardUtil.show();
        }
    }
    public void mousePressed(MouseEvent e) {
    }
    public void mouseReleased(MouseEvent e) {
    }
    public void mouseEntered(MouseEvent e) {
    }
    public void mouseExited(MouseEvent e) {
    }
}
