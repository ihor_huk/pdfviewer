package org.icepdf.ri.common.utility.util;

import java.io.File;
import java.io.IOException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.LPARAM;
import com.sun.jna.platform.win32.WinDef.WPARAM;
import com.sun.jna.win32.W32APIOptions;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ihor on 5/31/2017.
 */
public class TouchKeyboardUtil {
    private static final Logger LOGGER = Logger.getLogger(TouchKeyboardUtil.class.getCanonicalName());

    public interface User32 extends W32APIOptions {

        User32 instance = (User32) Native.loadLibrary("user32", User32.class, DEFAULT_OPTIONS);

        boolean ShowWindow(HWND hWnd, int nCmdShow);

        boolean SetForegroundWindow(HWND hWnd);

        HWND FindWindow(String winClass, String title);

        long GetWindowLong(HWND hwnd, int nIndex);

        int SendMessage(HWND hWnd, int msg, WPARAM wParam, LPARAM lParam);

        int SW_SHOW = 1;
    }

    private static void startOSK() {
        try {
            new ProcessBuilder("cmd", "/c", "OSK").start();
        }
        catch (IOException e) {
            LOGGER.log(Level.FINEST, "Can not execute OSK", e);
        }
    }

    public static void show() {
        User32 user32 = User32.instance;
        HWND hWnd = user32.FindWindow("OSKMainClass", null);
        if (hWnd == null) {
            startOSK();
        }
        else {
            user32.SendMessage(hWnd, 0x0112, new WPARAM(0xF120), new LPARAM());
        }
    }

    public static void hide() {
        User32 user32 = User32.instance;
        HWND hWnd = user32.FindWindow("OSKMainClass", null);
        if (hWnd != null) {
            user32.SendMessage(hWnd, 0x0112, new WPARAM(0xF020), new LPARAM());
        }
    }

    public static void toggle() {

        User32 user32 = User32.instance;
        HWND hWnd = user32.FindWindow("OSKMainClass", null);
        if (hWnd == null) {
            startOSK();
        }
        else {
            // (GetWidnowLong(hWnd, GWL_STYLE) & WS_MINIMIZE)
            // https://msdn.microsoft.com/en-us/library/windows/desktop/ms633584%28v=vs.85%29.aspx
            // https://msdn.microsoft.com/en-us/library/windows/desktop/ms632600%28v=vs.85%29.aspx
            if ((user32.GetWindowLong(hWnd, -16) & 0x20000000L) == 0) {
                user32.SendMessage(hWnd, 0x0112, new WPARAM(0xF020), new LPARAM());
            }
            else {
                user32.SendMessage(hWnd, 0x0112, new WPARAM(0xF120), new LPARAM());
            }
        }
    }

    public static void main(String args[]) {

        toggle();
    }

    static public Process startApp(String[] params) throws IOException {
        final File app = new File(params[0]);
        File workingDir = new File(".");
        try{
            final List<String> cmdArgs = new ArrayList<String>();
            cmdArgs.add(app.getAbsolutePath());
            cmdArgs.add(workingDir.getAbsolutePath());
            // Create a process, and start it.
            final ProcessBuilder p = new ProcessBuilder(cmdArgs);
            p.directory(new File("."));
            p.start();
        } catch (Throwable t) {
            t.printStackTrace(System.out);
        }

        return new ProcessBuilder(params).start();
    }
    static public Process startTouchKeyboard() throws IOException {

        String tabTip64Path = "C:\\Program Files\\Common Files\\microsoft shared\\ink\\TabTip.exe";
        String tabTip32Path = "C:\\Program Files (x86)\\Common Files\\Microsoft Shared\\Ink\\TabTip32.exe";

        String path = is64bit() ? tabTip64Path : tabTip32Path;
        String[] params = new String[1];
        params[0] = path;
        return startApp(params);
    }

    static public Process startOnScreenKeyboard() throws IOException {

        String appName = "osk.exe";

        String[] params = new String[1];
        params[0] = appName;
        return startApp(params);
    }

    static public boolean is64bit() {
        boolean is64bit = false;
        if (System.getProperty("os.name").contains("Windows")) {
            is64bit = (System.getenv("ProgramFiles(x86)") != null);
        } else {
            is64bit = (System.getProperty("os.arch").indexOf("64") != -1);
        }
        return is64bit;
    }
}
