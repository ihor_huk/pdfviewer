package org.icepdf.ri.common;

import com.sun.java.swing.plaf.windows.WindowsFileChooserUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;

/**
 * Created by Ihor on 7/4/2017.
 */
public class FSFileChooser extends JFileChooser
{

    public FSFileChooser()
    {
        super();
        disableFileNameTextField();
    }

    private void disableFileNameTextField() {
        WindowsFileChooserUI ui = (WindowsFileChooserUI)getUI();
        try {
            Field field = WindowsFileChooserUI.class.getDeclaredField("filenameTextField");
            field.setAccessible(true);
            JTextField tf= (JTextField) field.get(ui);
            tf.setEditable(false);
            tf.setEnabled(false);
            field.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    private static final long serialVersionUID = 1L;
}
