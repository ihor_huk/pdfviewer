package org.icepdf.ri.common.utility.util;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Ihor on 6/1/2017.
 */
public class PopupPrintClickMouseListener extends MouseAdapter {
    public void mousePressed(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    private void doPop(MouseEvent e){
        PopUpDemo menu = new PopUpDemo();
        menu.show(e.getComponent(), e.getX(), e.getY());
    }
}
