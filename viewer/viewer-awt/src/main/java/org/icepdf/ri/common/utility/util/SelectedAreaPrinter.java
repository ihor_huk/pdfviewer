package org.icepdf.ri.common.utility.util;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.icepdf.core.pobjects.PDimension;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.pobjects.PageTree;
import org.icepdf.core.pobjects.graphics.text.LineText;
import org.icepdf.core.pobjects.graphics.text.PageText;
import org.icepdf.ri.common.ViewModel;
import org.icepdf.ri.common.utility.util.print.PrintUtil;

import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.standard.MediaSizeName;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by Ihor on 6/1/2017.
 */
public class SelectedAreaPrinter implements Printable, ActionListener{

    private PageTree pageTree;
    private Page currentPage;
    private JFrame frameToPrint;
    private MediaSizeName mediaSizeName;

    public SelectedAreaPrinter(PageTree pageTree, MediaSizeName mediaSizeName) {
        this.pageTree = pageTree;
        this.mediaSizeName = mediaSizeName;
    }

    private String obtainSelectedText() {
        if(Objects.isNull(pageTree)) {
            return " ";
        }
        HashMap pageMap = new HashMap();
        StringBuilder currentPageSelectedText = new StringBuilder();
        for(int i = 0; i < pageTree.getNumberOfPages(); i++) {
            Page page = pageTree.getPage(i);
            String text = "";
            try {
                text = page.getViewText().getSelected().toString();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(!text.isEmpty()) {
                currentPage = page;
                pageMap.put(String.format("Page%s", i), page);
                currentPageSelectedText.append(text);
            }
        }

        return currentPageSelectedText.toString();
    }

    private void obtainSelectedPage() {
        if(Objects.isNull(pageTree)) {
            return;
        }
        for(int i = 0; i < pageTree.getNumberOfPages(); i++) {
            Page page = pageTree.getPage(i);
            String text = "";
            try {
                text = page.getViewText().getSelected().toString();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(!text.isEmpty()) {
                currentPage = page;
            }
        }


    }

    private String[] selectedLines() throws InterruptedException {
        obtainSelectedPage();
        return currentPage.getViewText().getSelected().toString().split("\n");
    }

    private BufferedImage obtainCroppedImage() throws IOException {
        File pdfFile = ViewModel.getDefaultFile();
        if(Objects.isNull(pdfFile)) {
            return null;
        }

        PDDocument doc  = PDDocument.load(pdfFile);
        PDFRenderer renderer = new PDFRenderer(doc);
        BufferedImage image = renderer.renderImage(currentPage.getPageIndex());
        doc.close();
        Point startPoint = resolveStartPoint(currentPage.getStartSelectionPoint());
        Point endPoint = resolveEndPoint(currentPage.getEndSelectionPoint());
        int xPos = startPoint.x > endPoint.x ? endPoint.x : startPoint.x;
        int yPos = startPoint.y > endPoint.y ? endPoint.y : startPoint.y;
        int width = Math.abs(startPoint.x - endPoint.x);
        int height = Math.abs(startPoint.y - endPoint.y);
        return image.getSubimage(xPos, yPos, width, height);
    }

    private Point resolveEndPoint(Point endSelectionPoint) {
        if(Objects.nonNull(endSelectionPoint) ) {
            return endSelectionPoint;
        }

        PDimension pdimension = currentPage.getSize(0);
        return Objects.nonNull(pdimension) ? new Point((int) pdimension.getWidth(), (int) pdimension.getHeight()) : new Point(0, 0);
    }

    private Point resolveStartPoint(Point startSelectionPoint) {
        return Objects.nonNull(startSelectionPoint) ? startSelectionPoint : new Point(0, 0);
    }

    private void  createPrintArea() throws InterruptedException {
        String[] sLines = selectedLines();
        frameToPrint = new JFrame("Selected text");
        JTextArea text = new JTextArea(sLines.length, 200);
        text.append(currentPage.getViewText().getSelected().toString());
        JScrollPane pane = new JScrollPane(text);
        pane.setPreferredSize(new Dimension(250,200));
        frameToPrint.add("Center", pane);
    }

    public int print(Graphics g, PageFormat pf, int page) throws
            PrinterException {

        if (page > 0) { /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }

        /* User (0,0) is typically outside the imageable area, so we must
         * translate by the X and Y values in the PageFormat to avoid clipping
         */
        Graphics2D g2d = (Graphics2D)g;
        g2d.translate(pf.getImageableX(), pf.getImageableY());

        try {
            createPrintArea();
            frameToPrint.pack();
            frameToPrint.printAll(g);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        /* Now print the window and its visible contents */


        /* tell the caller that this page is part of the printed document */
        return PAGE_EXISTS;
    }

    public void actionPerformed(ActionEvent e) {
        obtainSelectedPage();
        try {
            String textToPrint = currentPage.getViewText().getSelected().toString();
            PrintService service = PrintServiceLookup.lookupDefaultPrintService();
            PrintUtil.print(service, textToPrint, "\n");//, mediaSizeName);
            //PrintUtil.print(obtainCroppedImage());//TODO:
        }
        catch (InterruptedException | PrintException | IOException e1) {
            e1.printStackTrace();
        }
//        catch (InterruptedException | IOException e1) {
//            e1.printStackTrace();
//        }
    }

    private Rectangle obtainSelectedArea() {
        Rectangle rect = currentPage.getPageShape(20, 0, 100).getBounds();
        Point startPoint = currentPage.getStartSelectionPoint();
        Point endPoint = currentPage.getEndSelectionPoint();
        int xPos = startPoint.x > endPoint.x ? endPoint.x : startPoint.x;
        int yPos = startPoint.y > endPoint.y ? endPoint.y : startPoint.y;
        int width = Math.abs(startPoint.x - endPoint.x);
        int height = Math.abs(startPoint.y - endPoint.y);
        Rectangle viewRect = new Rectangle(xPos,yPos,width,height);
        return viewRect;
    }

    public void actionPerformedOld(ActionEvent e) {
//        PrinterJob job = PrinterJob.getPrinterJob();
//        job.setPrintable(this);
//        boolean ok = job.printDialog();
//        if (ok) {
//            try {
//                job.print();
//            } catch (PrinterException ex) {
//              /* The job did not successfully complete */
//            }
//        }
    }

    public ArrayList<LineText> getPageLines() {
        obtainSelectedPage();
        if(Objects.isNull(currentPage)){
            return null;
        }
        PageText viewText = null;
        try {
            viewText = currentPage.getViewText();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return Objects.nonNull(viewText) ? viewText.getPageLines() : null;
    }

//    public static void main(String args[]) {
//
//        UIManager.put("swing.boldMetal", Boolean.FALSE);
//        JFrame f = new JFrame("Hello World Printer");
//        f.addWindowListener(new WindowAdapter() {
//            public void windowClosing(WindowEvent e) {System.exit(0);}
//        });
//        JButton printButton = new JButton("Print Hello World");
//        printButton.addActionListener(new SelectedAreaPrinter());
//        f.add("Center", printButton);
//        f.pack();
//        f.setVisible(true);
//    }
}
