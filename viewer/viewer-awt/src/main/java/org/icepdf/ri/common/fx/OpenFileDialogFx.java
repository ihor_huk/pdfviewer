package org.icepdf.ri.common.fx;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;

/**
 * Created by Ihor on 5/11/2017.
 */
public class OpenFileDialogFx {
    public static void initAndShowGUI() {
        // This method is invoked on Swing thread


        JFrame frame = new JFrame("FX");
        final JFXPanel fxPanel = new JFXPanel();
        frame.add(fxPanel);

        Platform.runLater(new Runnable() {

            public void run() {
                initFX(fxPanel);
            }
        });


    }

    public static File openFile(final String title) {
        new JFXPanel();
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(new Stage());
        return file;
    }

    private static void initFX(JFXPanel fxPanel) {
        // This method is invoked on JavaFX thread
        Scene scene = createScene();
        fxPanel.setScene(scene);

    }

    private static Scene createScene() {
        Group root = new Group();
        return new Scene(root, 500, 500, Color.BLACK);
    }
}
