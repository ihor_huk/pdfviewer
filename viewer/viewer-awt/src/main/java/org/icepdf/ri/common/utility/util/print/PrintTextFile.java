package org.icepdf.ri.common.utility.util.print;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import java.io.*;

/**
 * Created by Ihor on 6/2/2017.
 */
public class PrintTextFile {
    public static void main(String[] args) throws PrintException, IOException {
        fileMain();
    }

    public static void fileMain() throws IOException, PrintException {
        String defaultPrinter =
                PrintServiceLookup.lookupDefaultPrintService().getName();
        System.out.println("Default printer: " + defaultPrinter);

        PrintService service = PrintServiceLookup.lookupDefaultPrintService();

        String toPrint = "test print \n  page \n line 1 \n line2 \n line3";

        PrintUtil.print(service, toPrint, "\n");


        // send FF to eject the page
//        InputStream ff = new ByteArrayInputStream("\f".getBytes());
//        Doc docff = new SimpleDoc(ff, flavor, null);
//        DocPrintJob jobff = service.createPrintJob();
//        pjw = new PrintJobWatcher(jobff);
//        jobff.print(docff, null);
//        pjw.waitForDone();
    }

    public static void textMain() throws IOException, PrintException {
        String defaultPrinter =
                PrintServiceLookup.lookupDefaultPrintService().getName();
        System.out.println("Default printer: " + defaultPrinter);

        PrintService service = PrintServiceLookup.lookupDefaultPrintService();

        InputStream in = new ByteArrayInputStream("sssss dsdasdas \n wdqwdqwqwdq".getBytes());

        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        pras.add(new Copies(1));


        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc doc = new SimpleDoc(in, flavor, null);

        DocPrintJob job = service.createPrintJob();
        PrintJobWatcher pjw = new PrintJobWatcher(job);
        job.print(doc, pras);
        pjw.waitForDone();
        in.close();

        // send FF to eject the page
        InputStream ff = new ByteArrayInputStream("\f".getBytes());
        Doc docff = new SimpleDoc(ff, flavor, null);
        DocPrintJob jobff = service.createPrintJob();
        pjw = new PrintJobWatcher(jobff);
        jobff.print(docff, null);
        pjw.waitForDone();
    }
}
